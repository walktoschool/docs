
---

Attempting to have a first GitLab Pages doc

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->
Firstly, there was the idea.

Actually, no. Before that, there was the walking group. You see, my eldest child was about to start school, and in doing so there seemed to be this system in which the school "helped out" in creating a "walking group" for the kids to be able to walk to school, with the intention of having one of the parents[^*] among the group of kids accompany them on the way. 


Being normal and human, I really had no idea what this entailed - though logically I felt it made a lot of sense. In the path to independence lies reliance on peers rather than authority figures, so reducing the 1:1 individuality of following the child every day is actually beneficial. And we don't want to be accompanying her for all eternity either, in the not too distant future we want her to be getting to school unaccompanied - by foot (and maybe bike or hoverboard in the future, but let's keep the feet-on-the-ground motivation going here).

So, there we were, enjoying a nice summer, and start of school rapidly approaching. We knew which group we were in. We knew who was around. We just hadn't met them before, and had no idea who these people were and what they were like. I mean. I'm Norwegian. We don't just show up at random peoples places and interact for a common purpose without first being shoved in a crowded elevator together or something to kickstart the smalltalk.

So the bravest of the group decided to reach out to us - some of them already knew each other (the lucky bastards), but none knew us (being highest up the street - we had basically no idea there were even kids of that age around). As such, it was agreed that we all meet up at 18:00 to figure out this thing called walking groups. 

As with any such thing, as soon as we discovered that one of the group had _actually done this before_, it fell on them to describe and dictate how these things worked, what to expect, and share experiences. Essentially, there were a very few things that we covered:

1. Who are these kids that we are supposed to be walking with
2. When do we start walking
3. What do we do if not everyone has showed up
4. Where do we walk from
5. Which parent walks when

One last thing that was also fairly essential these days, was the question of _how do we communicate_, though that was fairly quickly cleared up (messenger seemed to be the least common denominator, so that was that).

We cleared up most of these thoughts, discussed the merits of the various alternatives, and ended up with these simple answers:

1. **kids**: They were playing in the background, we were told their names when we met up, and let's just hope we recognize their faces whenever it's our turn to walk them to school ;)
2. **when**: We ended up on 07:45 such that we would be at school in reasonable time.  This wasn't entirely ideal for everyone involved, but it was a workable time.
3. **no-show**: How long do we hang around waiting for stragglers? We don't. We walk at 07:45, anyone arriving later than that will just have to catch up or deliver the kids themselves. Additionally, if anyone *knows* they're not coming, a little message on the communication channel would be enough to let us know not to wait if we're all ready beforehand.
4. **where**: easy. garages at the bottom of the street, everyone knows where that is, and it's not too far.
5. **who-walks-when**: There were two main strategies proposed, weekly or daily rotations. Either the parents of a child would have a whole week of walking, and then rotated to the next parent for the next week and so on, or it would rotate on a daily basis. Considering we have 7 kids in the group, we eventually ended up on wanting to try a weekly rotation to start, and we'll see how it goes from there. It's no fun agreeing on daily rotation and having to remember that this week you walk on Tuesday, next week it's Thursday and so on. 

After the meeting, THAT is where the idea struck (ref. first line of this document).

These are all very simple parameters that are tricky to juggle, but should be relatively easy to code into a simple single-purpose app that just manages to deal with this one tiny little thing.  Keys to that are:

## Who am I supposed to be walking with

I'm not great with names. I'm also of the general consensus of all kids look alike until you get to know them. When I'm going to be doing my walking, I really want to make sure that I know who it is I'm walking with, and making sure that I'm not forgetting anyone (or kidnapping some unsuspecting kid who happened to be around the pickup area around the right time). 

This was also what really triggered the idea in the first place - I'd like to communicate my kids face and.. certain character traits.. to the rest of the group.


My Kid.|
-------|
(insert picture which is representative and hopefully not too crazy)|
Note: Walks like a friggin' granny. |
Motivated by: Fantasy. |
How to get her to move: Use fantasy to your advantage. Ask her to run like a horse. You are permitted to lie to her, as long as the lie is far-fetched enough. Tell her that you saw a unicorn at the end of a rainbow by the school. Suggest to her that today is finally the day where the whole school is made out of ice cream. |

It would be great if there was one place where I could check who these kids were and get some useful bits of info.

**_Secondly_**.. It would be good if it was possible to give a heads up to the group which days my child would not be walking. That way it's easy to see how many are supposed to be walking any given day. If the child is sick or on an extra vacation or something, it's a lot easier to just signify that in the tool than to remember exactly which kid is not going to be arriving that day.

## Which days do I walk

We ended up deciding to walk weekly. But which week? Who gets when? We agreed that one of us would use their Mad Excel Skillz to create a simple table with the kids name and which week their ~~superiors~~parents would be responsible for walking the group to school.  That was great, but I feel it's somewhat inflexible. Could do better with a google sheet where we could edit it or something, but that lacks some form of consensus as well.

Personally, I'm of the go-with-the-flow mentality and will take whatever leftover scraps are available as I don't feel I have any real preferences one way or another. However, I would like to:

- Know when I need to be the one walking
- Find some way of coordinating exceptions, such as if I'm supposed to be walking one week but I need someone to take the friday as we'll be going on a long weekend or something
- Be able to easily take over for someone who needs someone to fill in, without everyone trying to figure out if they can be the ones filling in that day

Some added bonuses to that would be to auto-generate a timetable, possibly including simple things such as known school holidays, and attempting to distribute the walking fairly within the group. Automated/suggested rescheduling should also be possible. Hey, for fun and fairness, could add some gamification to keep track of walks and good samaritans who consistently take one for the team (always nice to reward good behavior every now and then). 

Additionally, having this kind of system makes it a little bit difficult to accomodate all needs - if some people are really clear that they would like to walk every thursday, and the rest of the group are wanting to do it weekly, that's fine - but how does that distribute over time? What if another would like to take every other tuesday and the first monday of the month?  I'm not saying we'll solve that anytime soon, but if it's possible to find some relatively user-friendly ways of resolving and testing out these potential conflicts, that would be great.

# What this should not do.

What it does is important, but the constraints on what it should not are important as well. These constraints are likely to change over time, but for now, some things it should not do are:

- **Facilitate communication**.  I'm not building the next messenger, whatsapp, or discord for kid-needs-to-go-to-school-related communication.
  - _Rationale_: We have tools for this already, let's just use those.
- **Track your kids**. The implications and ramifications of geotracking the walking group is not really a road taken lightly. I might add a simple map with a pin to indicate where the group meets up - but realtime location and tracking is just not going to help. 
  - _Rationale_: We trust the members of the group walking with the kids, we don't need to see where that member is to know that the group is going to get to its destination. Sure, it might be useful in the "catching up with the group" scenario, but you have phones. Call. Use the communcication tool above. Or just deal with it for now ;)

# How this should do what it does

Some very basic ground principles in designing this application are as follows. They are not completely thought out yet, but the ideal is something I'll strive to do as much as possible.

## I do not want to be able to read any information out of the database about anything.

Guess what. This system will be storing information about kids and other people. While GDPR is a concern for many, my main concern is privacy in general (with GDPR being a good subset of that mindset - and one which I intend to follow to the letter, and then some). 

So - I would like to design the system such that:

- Users log in through some form of OAUTH so that I do not store passwords or more data than I really need in the database.
- Users create their own group(s), and invite other users into them.
- Information stored in the groups (such as information on children, who will be where when, availabilities, times, etc) are only available to the members of that group.

For me, this means relying on some form of encryption and key-sharing. It's going to be fun to figure out exactly how this works.

> One issue in my mind at the moment is that as a database owner/admin, I will be able to see and manage user-group relationships, and thereby add myself or anyone else into every group. If the user-group relationship is not the only key needed to access the encrypted data, then that helps a lot - but really need to see if it's possible to have user-group relationships in a database that are not visible or modifiable in that database.. heh. 

## I want the following key functions

### Users

The user should be able to log in using some standard OAUTH provider, such as Facebook and Google. It would likely be trivial to add stuff like github, gitlab, patreon and whatnot - but considering the market, Facebook and Google are sufficient.

For those people who really want to remain anonymous on the net, I'll find a good security-focused oauth provider that one can register at as a third option.

### Groups

A user should be able to create a group and invite other users into it. Any user is considered a full member of the group, so no "group admin" stuff at the moment, we'll be nicely egalitarian.

### Kids

A child is a member of a group, and has a relationship to one or more users. The child exists only in that group (including the information relating to the child), and thus the relationship to that child exists only in that group. 

> Is it necessary to place a restriction on who can edit a child? If I edit someone elses child, would it not be for a good reason? Prevention of malice should not be a reason to create a mandatory restriction in this case, but if it's "easy to implement" I'll have a look at it.

A simple place where members of the group can see the following about the kids who are supposed to be walking:

- Name
- Picture
- Free-form notes
  - Stuff like birthdays, likes, dislikes, nicknames, and other stuff can go in there. 

### Relationship to other users

In a lot of cases, there will not be one single individual who is responsible for fulfilling the obligations as designated walker - there are likely more users related to each child.  As a design choice, we'll have the calendar according to child rather than the user doing the walking (i.e. it is Child A's parents who are walking next week, we don't care which one of you does it, you figure it out). 

> Future design considerations could take into account a form of nested calendar where the child is the main element, and within the childs time it can be delegated between users for finer granularity. Overkill, in other words, but I see some cases for it.

### Calendar

This is a biggie. There should be two dimensions to the calendar:

#### Planning who is walking

The planning part of who is walking should be such that there's an easily readable calendar indicating who is responsible for taking the kids which day/week/month/epoch.

It would be good if this calendar had a global "no walking days", defaulting to not walking on any saturday or sunday, but also allowing adding of school vacation, etc. 

> **NOTE!** This part needs a LOT more writing in it, but I'm running out of time ;)

#### When the kid is not coming

By default the kid is expected to be walking every day. Should there be any reason why the kid isn't, it should be possible to give an indication in the app that the kid isn't showing up (so that you know that when you have 6/7 kids, you shouldn't hang around for the last one).  Also useful if there are things like seasonal difficulties in getting around, so that you can block off certain months or suchlike.

# More to come..

Alas, time has run out on this first draft, but I'll get back to it.


[^*]: A note on my use of the word "parent" in this (and probably all other) documents. When you read `parent` read it as its logical/programmatic interpretetion - something which has a higher relation than that which is below.  In legalese, this would probably translate to (familial) parent, guardian, the local cheesemonger, or whomever else has an authorized and accepted reason to escort other individuals to a designated place of reception. In all likelihood, neighbours, friends, grandparents, and others will all be likely candidates to be a part of a walking group, so let's just say that a parent is a logical ancestor of a child and use our imagination to fill in whatever interpretation of parent we feel is justified for ourselves. 